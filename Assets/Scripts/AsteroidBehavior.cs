﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehavior : MonoBehaviour
{

    public enum AsternoidType
    {
        Big,
        Normal,
        Small
    }

    const float MaxAsternoidSize = 4.0f;
    const float AsternoidScaleDown = 1.0f;
    public float DistanceToBreakDown = 1.5f;

    public float m_Speed;

    private Vector2 m_MovingDirection;

    private AsternoidType m_Type;

    // Use this for initialization
    void Start()
    {
        //Range on percentage is more easier to control

        m_MovingDirection.x = Random.Range(-1.0f, 1.0f);
        m_MovingDirection.y = Random.Range(-1.0f, 1.0f);

        m_Speed = Random.Range(0.1f, 1.5f);

        float Percentage = Random.value;

        if (Percentage <= 0.5)//p 50% small
            m_Type = AsternoidType.Small;
        else if (Percentage > 0.5 && Percentage <= 0.85)//50% < p < 85% 
            m_Type = AsternoidType.Normal;
        else
            m_Type = AsternoidType.Big;

        Debug.Log(m_Type + "-" + Percentage+"-"+ m_Speed+"-"+ m_MovingDirection);
        SetAsternoidSize();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(m_MovingDirection * m_Speed, ForceMode2D.Force);
        WallCheck();
    }

    public void SetAsternoidType(AsternoidType type)
    {
        m_Type = type;
        Debug.Log(m_Type);
    }


    public void SetAsternoidSize()
    {
        Debug.Log(m_Type);
        float ScaleSize = MaxAsternoidSize - (float)m_Type * AsternoidScaleDown;
        gameObject.transform.localScale = new Vector3(ScaleSize, ScaleSize, ScaleSize);
    }

    private void WallCheck()
    {
        if (gameObject.transform.position.x >= 13)
            gameObject.transform.position = new Vector3(-13, gameObject.transform.position.y);
        else if (gameObject.transform.position.x <= -13)
            gameObject.transform.position = new Vector3(13, gameObject.transform.position.y);
        if (gameObject.transform.position.y >= 7)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, -4);
        else if (gameObject.transform.position.y <= -4)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 7);
    }

    private void BrokeDownAsternoid()
    {
        switch (m_Type)
        {
            case AsternoidType.Big:
                {
                    SpawnAsternoidAtAndWithType(new Vector3(gameObject.transform.position.x- DistanceToBreakDown,
                                                            gameObject.transform.position.y- DistanceToBreakDown,
                                                            gameObject.transform.position.z),
                                                            AsternoidType.Normal);
                    SpawnAsternoidAtAndWithType(new Vector3(gameObject.transform.position.x + DistanceToBreakDown,
                                                            gameObject.transform.position.y - DistanceToBreakDown,
                                                            gameObject.transform.position.z),
                                                            AsternoidType.Normal);
                    SpawnAsternoidAtAndWithType(new Vector3(gameObject.transform.position.x - DistanceToBreakDown,
                                                            gameObject.transform.position.y + DistanceToBreakDown,
                                                            gameObject.transform.position.z),
                                                            AsternoidType.Normal);
                    SpawnAsternoidAtAndWithType(new Vector3(gameObject.transform.position.x + DistanceToBreakDown,
                                                            gameObject.transform.position.y + DistanceToBreakDown,
                                                            gameObject.transform.position.z),
                                                            AsternoidType.Normal);
                }
                break;
            case AsternoidType.Normal:
                {
                    SpawnAsternoidAtAndWithType(new Vector3(gameObject.transform.position.x - DistanceToBreakDown,
                                                            gameObject.transform.position.y,
                                                            gameObject.transform.position.z),
                                                            AsternoidType.Small);

                    SpawnAsternoidAtAndWithType(new Vector3(gameObject.transform.position.x + DistanceToBreakDown,
                                                            gameObject.transform.position.y,
                                                            gameObject.transform.position.z),
                                                            AsternoidType.Small);
                }
                break;
            case AsternoidType.Small:
                break;
            default:
                break;
        }
    }

    private void SpawnAsternoidAtAndWithType(Vector3 position, AsternoidType asternoidType)
    {
        //GameObject Asternoid = Instantiate(m_AsternoidPrefab, position, Quaternion.identity);
        //Asternoid.GetComponent<AsteroidBehavior>().SetAsternoidType(asternoidType);
        //Asternoid.GetComponent<AsteroidBehavior>().SetAsternoidSize();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Asternoid - " + collision.gameObject.name);
        if (collision.gameObject.name.Contains("Projectile"))
        {
            //BrokeDownAsternoid();
            AsternoidManager.m_Instance.m_Asternoids.Remove(gameObject);
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }

}
