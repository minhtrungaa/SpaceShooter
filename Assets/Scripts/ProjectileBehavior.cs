﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    public float m_ProjectileAcceleration;

    Vector2 m_ProjectileFinalVelocity;

    // Use this for initialization
    void Start()
    {

    }

    public void PostStart(Vector2 shipFacingDirection)
    {
        m_ProjectileFinalVelocity = shipFacingDirection.normalized * m_ProjectileAcceleration;
    }

    // Update is called once per frame
    void Update()
    {
        //m_ProjectileVelocity += m_ProjectileDirection;
        gameObject.transform.position += new Vector3(m_ProjectileFinalVelocity.x, m_ProjectileFinalVelocity.y) * Time.deltaTime;

        WallCheck();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("ProjectileBehavior Trigger - " + collision.gameObject.name);
        if (collision.gameObject.name.Contains("Asternoid"))
        {
            Destroy(gameObject);
        }
    }

    private void WallCheck()
    {
        if (gameObject.transform.position.x >= 13)
            Destroy(this.gameObject);
        else if (gameObject.transform.position.x <= -13)
            Destroy(this.gameObject);
        if (gameObject.transform.position.y >= 7)
            Destroy(this.gameObject);
        else if (gameObject.transform.position.y <= -4)
            Destroy(this.gameObject);
    }
}
